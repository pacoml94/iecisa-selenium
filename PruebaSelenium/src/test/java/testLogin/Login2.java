package testLogin;

import static org.junit.Assert.assertTrue;

import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Login2 {

	private WebDriver driver;
	
	
	@Before
	public void setUp() throws Exception {
		WebDriverManager.chromedriver().version("77.0.3865.40").setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		String projectPath = System.getProperty("user.dir");
		System.out.println(projectPath);
		//System.setProperty("webdriver.chrome.driver", projectPath + "\\drivers\\chromedriver.exe" );
		//driver = new ChromeDriver();
		String url = "http://automationpractice.com/index.php";
		driver.manage().window().maximize();
		driver.get(url);
	}

	//@Test
	public void test() throws InterruptedException {
		WebElement txtFind = driver.findElement(By.id("search_query_top"));
		txtFind.click();
		txtFind.sendKeys("pantalones");
		Thread.sleep(2000);
		WebElement submit = driver.findElement(By.xpath("//button[@name='submit_search']"));
		submit.submit();
		Thread.sleep(2000);
	}
	
	@Test
	public void testRegistro() throws InterruptedException {
		WebElement signIn = driver.findElement(By.xpath("//a[@class='login']"));
		signIn.click();
		Thread.sleep(2000);
		WebElement email = driver.findElement(By.id("email_create"));
		email.click();
		Thread.sleep(2000);
		email.sendKeys("hola@q.com");
		Thread.sleep(2000);
		WebElement submit = driver.findElement(By.xpath("//button[@id='SubmitCreate']"));
		submit.submit();
		Thread.sleep(2000);
		WebElement assertText = driver.findElement(By.xpath("//span[@class='navigation_page']"));
		System.out.println(assertText.getText());
		assertTrue(assertText.getText().contentEquals("Authentication"));
		
		String ventana = driver.getWindowHandle();
		Set<String> ventana2 = driver.getWindowHandles();
		Iterator <String> i2 = ventana2.iterator();
		
		while(i2.hasNext()) {
			String p2 = i2.next();
			ventana.equals(p2);
			driver.switchTo().window(p2);
		}
		List<WebElement> formulario = driver.findElements(By.xpath("//form//div//input"));
		String[] form = new String[formulario.size()];
		System.out.println("cantidad" +formulario.size());
		
		
		int i = 0;
		for(WebElement e : formulario) {
			Thread.sleep(3000);
			form[i] = e.getText();
			if(i <=2) {
				e.sendKeys("Jose");
			} else if (i <=3) {
				e.sendKeys("pescobar@zltech.es");
			} else if (i == 5) {
				e.sendKeys("pass");
			}
			
			/*
				 * else if(i <=4) { e.sendKeys("638546467"); } else if(i <=5) {
				 * e.sendKeys("Militar"); } else if(i <=6) {
				 * e.sendKeys("es una prueba de automatización"); } else if (i <=7) { Select
				 * interes = new
				 * Select(driver.findElement(By.xpath("//select[@id='product_interests']")));
				 * interes.selectByValue("Information Governance");
				 * 
				 * } else if (i <= 8) { Select question = new
				 * Select(driver.findElement(By.xpath(
				 * "//select[@id='9872_348852pi_9872_348852']")));
				 * question.selectByValue("true"); } else if (i <= 9) { e.click();
				 * 
				 * }
				 */
			
			
			i ++;}
		Thread.sleep(3000);
		WebElement submit2 = driver.findElement(By.xpath("//*[@id=\"submitAccount\"]"));
		submit2.submit();
		////form//div/input -> para los inputs
		/**
		
		int j = 0;
		for(WebElement e : formulario) {
			Thread.sleep(3000);
			form[j] = e.getText();
			System.out.println(form[j]);
			if (j <= 0) {
				
				e.click();
				System.out.println(e.getText());
			} else if (j <= 2) {
				e.click();
				e.sendKeys("Murillo");
			} else if (j <= 3) {
				e.click();
				e.sendKeys("Francisco");
			} else if (j <= 4) {
				e.click();
			} else if (j <= 5) {
				e.click();
				e.sendKeys("1234");
			}
			
			j++;
		}
		*/
	}
	
	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.close();
	}
	



}

package twitter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginTwitter {
	
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		String projectPath = System.getProperty("user.dir");
		System.out.println(projectPath);
		System.setProperty("webdriver.chrome.driver", projectPath + "\\drivers\\chromedriver.exe" );
		driver = new ChromeDriver();
		String url = "http:/google.com";
		driver.manage().window().maximize();
		driver.get(url);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void test() throws InterruptedException {
		WebElement txtFind = driver.findElement(By.name("q"));
		txtFind.sendKeys("twitter");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@class='gNO89b'][1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//h3[@class='LC20lb'])[1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//a[@class='js-nav EdgeButton EdgeButton--medium EdgeButton--primary StaticLoggedOutHomePage-buttonSignup'])")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@name='name']"))
		.sendKeys("chess25.madrid");
	}

}

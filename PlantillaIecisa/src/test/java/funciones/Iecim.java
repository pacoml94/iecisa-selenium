package funciones;


import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Iecim {
	// marcamos una clase 5 segundos para reutilizarlos
	public static void sleepSeconds(int seconds) {

		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//hover
	public static void move(WebDriver driver, String name) {
		Actions actions = new Actions(driver);
		WebElement menuHoverLink = driver.findElement(By.linkText(name));
		actions.moveToElement(menuHoverLink);
		
	}
	
		//ventana nueva 
		public static void newVentana(WebDriver driver) {
			
			String ventana = driver.getWindowHandle();
			Set<String> ventana2 = driver.getWindowHandles();
			Iterator <String> i2 = ventana2.iterator();
			
			while(i2.hasNext()) {
				String p2 = i2.next();
				ventana.equals(p2);
				driver.switchTo().window(p2);
			}
		}
}

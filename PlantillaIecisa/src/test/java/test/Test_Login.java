package test;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import funciones.Screenshooter;
import funciones.SoutTest;
import pagesLogin.PO_Formulario;
import pagesLogin.PO_Login;

public class Test_Login {
	protected WebDriver driver;
	@BeforeMethod
	public void setUpTest() throws Exception {
		// cargamos el directorio para poder navegar
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver",
				projectPath + "\\src\\main\\resources\\drivers\\chromedriver.exe");
		// ejecuta sin interface grafica
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--headless");

		driver = new ChromeDriver(chromeOptions);

		String url = "http://automationpractice.com/index.php";
		// navegamos a google
		driver.get(url);
	
	}

	@Test(description = "prueba registro")
	public void registro () {
		PO_Login poLogin = new PO_Login(driver);
		poLogin.registrar("manu.iecisa@hotmail.com");
		PO_Formulario formulario = new PO_Formulario(driver);
		formulario.formulario("Manuel","Gac�a");
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		SoutTest.SouTest(result);
		if (!result.isSuccess()) {
			Screenshooter.takeScreenshot("CapturasError\\loginNew\\Error", driver);

		}
		driver.close();
	
	}
}
